# Peoples

## Table of contents
* [General Info](#general-info)
* [Installation](#installation)
* [Running Locally](#running-locally)
* [Build](#build)

## General Info
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Installation
```
npm install
```
This command will install all modules listed as dependencies in `package.json`.

## Running Locally
In the project directory, you can run:

```
npm start
```
* Runs the app in the development mode.
* Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
* The page will reload if you make edits.
* You will also see any lint errors in the console.

## Build
```
npm run build
```
* Builds the app for production to the `build` folder.
* It correctly bundles React in production mode and optimizes the build for the best performance.
* The build is minified and the filenames include the hashes.