import { useEffect, useRef, useState } from "react";

const defaultUsers = [
    {
        "id": "kped1o70",
        "name": "Francis Rutledge",
        "isFavorite": true,
        "color": "hsl(248, 30%, 57%)"
    },
    {
        "id": "kped1o66",
        "name": "Holt Delacruz",
        "isFavorite": true,
        "color": "hsl(227, 48%, 44%)"
    },
    {
        "id": "kped1o72",
        "name": "Maryann Gilbert",
        "isFavorite": false,
        "color": "hsl(170, 57%, 51%)"
    },
    {
        "id": "kped1o6u",
        "name": "Rutledge Phelps",
        "isFavorite": true,
        "color": "hsl(118, 55%, 35%)"
    },
    {
        "id": "kped1o7g",
        "name": "Wilkerson Reyes",
        "isFavorite": false,
        "color": "hsl(106, 46%, 39%)"
    }
]

const rand = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;

const toCapitalize = str => str.split(' ').filter(f => f).map(n => n[0].toUpperCase() + n.substr(1)).join(' ')


function App() {
    let userData = localStorage.getItem('users')
    let [users, setUsers] = useState(JSON.parse(userData) || defaultUsers)
    let [search, setSearch] = useState('')
    let [currentPage, setCurrentPage] = useState(0)
    let [activeTab, setActiveTab] = useState('ALL')
    let [confirmDelete, setConfirmDelete] = useState(null)
    let inputElm = useRef(null)

    useEffect(() => {
        localStorage.setItem('users', JSON.stringify(users))
    }, [users])
    useEffect(() => {
        setCurrentPage(0)

    }, [search, activeTab])

    const toggleFavorite = id => {
        let inx = users.findIndex(f => f.id === id)
        users[inx]['isFavorite'] = !users[inx]['isFavorite']
        setUsers([...users])
    }

    const deleteUser = () => {
        let inx = users.findIndex(f => f.id === confirmDelete)
        users.splice(inx, 1)
        setUsers([...users])
        setConfirmDelete(null)
    }

    const addNewUser = (e) => {
        e.preventDefault()
        let checkInx = users.findIndex(f => f.name.toLowerCase() === search.toLowerCase())
        if (search !== '' && checkInx === -1 && search.length >= 3) {
            let user = {
                id: (new Date().getTime() + rand(0, 100)).toString(36),
                name: toCapitalize(search),
                isFavorite: false,
                color: `hsl(${rand(0, 255)}, ${rand(30, 70)}%, ${rand(30, 70)}%)`
            }
            let newList = [...users, user].sort((a, b) => a.name > b.name ? 1 : -1)
            setUsers([...newList])
        }
    }


    let searchRegx = new RegExp(`${search.split(' ').filter(f => f).join('|')}`, 'gi')

    let filteredUsers = users.filter(f => f.name.match(searchRegx)).filter(f => activeTab === 'FAV' ? f.isFavorite : true)

    let nextPage = () => {
        let nextPos = currentPage + 4
        if (filteredUsers.length > nextPos) {
            setCurrentPage(nextPos)
        }
    }
    let PrevPage = () => {
        let nextPos = currentPage - 4
        if (nextPos >= 0) {
            setCurrentPage(nextPos)
        }
    }
    return (
        <>
            <div className="app">
                <h3 className="title">Peoples</h3>
                <form className="search-box" onSubmit={addNewUser}>
                    <div className="icon" onClick={() => inputElm.current && inputElm.current.focus()}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
                            <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                        </svg>
                    </div>
                    <input
                        value={search}
                        onChange={({ target }) => setSearch(target.value)}
                        ref={inputElm}
                        className="search"
                        placeholder="Search Here"
                    />
                    {
                        search !== '' && (
                            <div className="clear" onClick={() => setSearch('')}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" viewBox="0 0 16 16">
                                    <path d="M1.293 1.293a1 1 0 0 1 1.414 0L8 6.586l5.293-5.293a1 1 0 1 1 1.414 1.414L9.414 8l5.293 5.293a1 1 0 0 1-1.414 1.414L8 9.414l-5.293 5.293a1 1 0 0 1-1.414-1.414L6.586 8 1.293 2.707a1 1 0 0 1 0-1.414z" />
                                </svg>
                            </div>
                        )
                    }
                </form>
                <div className="tabs">
                    <div
                        className={`tab ${activeTab === 'ALL' ? 'active' : ''}`}
                        onClick={() => setActiveTab('ALL')}
                    >
                        All Peoples
                </div>
                    <div
                        className={`tab ${activeTab === 'FAV' ? 'active' : ''}`}
                        onClick={() => setActiveTab('FAV')}
                    >
                        Favorites
                </div>
                </div>
                <div className="user-list">
                    {
                        filteredUsers.slice(currentPage, currentPage + 4).map(({ id, name, isFavorite, color }, inx) => {
                            return (
                                <div className="user-details" key={inx}>
                                    <div
                                        className="profile"
                                        style={{ backgroundColor: color }}
                                    >
                                        {name.split(' ').map(f => f[0]).join('')}
                                    </div>
                                    <div className="content">
                                        <div
                                            dangerouslySetInnerHTML={{ __html: search === '' ? name : name.replace(searchRegx, match => `<mark>${match}</mark>`) }}
                                        ></div>
                                        <small>is your friend</small>
                                    </div>
                                    <div className="actions ">
                                        <div className={`action favorite ${isFavorite ? 'active' : ''}`} onClick={() => toggleFavorite(id)}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
                                                <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                                            </svg>
                                        </div>
                                        <div className="action delete" onClick={() => setConfirmDelete(id)}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
                                                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                                <path fillRule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
                <div className="pagination">
                    <div className={`pagination-item ${currentPage === 0 ? 'disabled' : ''}`} onClick={PrevPage}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" viewBox="0 0 16 16">
                            <path fillRule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z" />
                        </svg>
                    </div>
                    <div className={`pagination-item ${filteredUsers.length < currentPage + 4 ? 'disabled' : ''}`} onClick={nextPage}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" viewBox="0 0 16 16">
                            <path fillRule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                        </svg>
                    </div>
                </div>
            </div>
            {
                confirmDelete !== null && (
                    < div className="modal-wrapper">
                        <div className="modal">
                            <div className="modal-content">
                                <div className="icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" fill="currentColor" viewBox="0 0 16 16">
                                        <path d="M6.95.435c.58-.58 1.52-.58 2.1 0l6.515 6.516c.58.58.58 1.519 0 2.098L9.05 15.565c-.58.58-1.519.58-2.098 0L.435 9.05a1.482 1.482 0 0 1 0-2.098L6.95.435zm1.4.7a.495.495 0 0 0-.7 0L1.134 7.65a.495.495 0 0 0 0 .7l6.516 6.516a.495.495 0 0 0 .7 0l6.516-6.516a.495.495 0 0 0 0-.7L8.35 1.134z" />
                                        <path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z" />
                                    </svg>
                                </div>
                                <h4>Are you sure?</h4>
                                <div>You will not be able to recover this person details!</div>
                            </div>
                            <div className="modal-footer">
                                <div className="option" onClick={() => setConfirmDelete(null)}>No</div>
                                <div className="option" onClick={deleteUser}>Yes, delete it!</div>
                            </div>
                        </div>
                        <div className="backdrop" onClick={() => setConfirmDelete(null)}></div>
                    </div>
                )
            }
        </>
    );
}

export default App;
